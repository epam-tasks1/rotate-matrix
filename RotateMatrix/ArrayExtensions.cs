﻿using System;

#pragma warning disable CA1814
#pragma warning disable S2368

namespace RotateMatrix
{
    public static class ArrayExtensions
    {
        public static void Rotate90DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[j, i];
                    matrix[j, i] = tmp;
                }
            }

            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                for (int j = 0; j < matrix.GetLength(1) / 2; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[i, matrix.GetLength(1) - j - 1];
                    matrix[i, matrix.GetLength(1) - j - 1] = tmp;
                }
            }
        }

        public static void Rotate90DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[j, i];
                    matrix[j, i] = tmp;
                }
            }

            for (int i = 0; i < matrix.GetLength(1) / 2; i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[matrix.GetLength(1) - i - 1, j];
                    matrix[matrix.GetLength(1) - i - 1, j] = tmp;
                }
            }
        }

        public static void Rotate180DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                for (int j = 0; j < matrix.GetLength(1) / 2; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[i, matrix.GetLength(1) - j - 1];
                    matrix[i, matrix.GetLength(1) - j - 1] = tmp;
                }
            }

            for (int i = 0; i < matrix.GetLength(1) / 2; i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[matrix.GetLength(1) - i - 1, j];
                    matrix[matrix.GetLength(1) - i - 1, j] = tmp;
                }
            }
        }

        public static void Rotate180DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rotate180DegreesClockwise(matrix);
        }

        public static void Rotate270DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rotate90DegreesCounterClockwise(matrix);
        }

        public static void Rotate270DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            Rotate90DegreesClockwise(matrix);
        }

        public static void Rotate360DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
        }

        public static void Rotate360DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
        }
    }
}
